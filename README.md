# keebs

A collection of my personal keyboard customizations.

## Getting started

Remapping keys is done with [Via](https://usevia.app/#/) using a Chromium based browser.

In Linux, connecting Via to the keyboard may fail. This is due to permissions on the HID device.

In a new tab, go to `chrome://device-log/` and look for an error similar to the following:

```
[17:41:40] Failed to open '/dev/hidraw3': FILE_ERROR_ACCESS_DENIED
[17:41:40][ Access denied opening device read-write, trying read-only.
```

To grant Via access to your keyboard, do the following:

```
sudo chmod a+rw /dev/hidraw3
```

Once you are done remapping keys in Via, reset the permissions

```
sudo chmod 600 /dev/hidraw3
```

## Configuring the correct layers

For Keychron keyboards with the switch set to Windows only layers 1, 3 & 4 may be remapped. Layers 0, 2, 4 are for Mac (with layer 4 being shared).