# Keychron Q4

My daily driver.

## Via layout files

The file `q4_us_v1.6.json` is Keychron's official layout schema.

Other files are my own key mapping experiments.