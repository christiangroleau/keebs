# Keychron K9 Pro

My travel keyboard.

## Via layout files

The file `k9_pro_ansi_rgb_v1.00.json` is Keychron's official layout schema.

Other files are my own key mapping experiments.